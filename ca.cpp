#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <stdbool.h>

#define DIM_X 30
#define DIM_Y 30
#define PXL_X 30
#define PXL_Y 30
#define TAB_OFFSET 1
#define FRAMERATE 100

typedef struct {
    int x;
    int y;
} pixel;

void pause();
int processAC1(bool tab[][DIM_Y+(TAB_OFFSET*2)], long count);
int processAC2(bool tab[][DIM_Y+(TAB_OFFSET*2)], bool res[][DIM_Y+(TAB_OFFSET*2)])
{
    int i, j, test;
    for (i = TAB_OFFSET ; i < DIM_X+TAB_OFFSET ; i++)
    {
        for (j = TAB_OFFSET ; j < DIM_Y+TAB_OFFSET ; j++)
        {
            test = tab[i-1][j-1] + tab[i][j-1] + tab[i+1][j-1] + tab[i-1][j] + tab[i+1][j] + tab[i-1][j+1] + tab[i][j+1] + tab[i+1][j+1];
            if(test > 1)
            {
                printf("%d-%d::%d\n",i,j,test);
            }
            if( tab[i][j] == true )
            {
                if( test == 2 )
                {
                    printf("a%d\n",test);
                    res[i][j] = true;
                }
                else if( test == 3 )
                {
                    printf("b%d\n",test);
                    res[i][j] = true;
                }
                else
                {
                    res[i][j] = false;
                }
            }
            else if( test == 3)
            {
                printf("c%d\n",test);
                res[i][j] = true;
            }
        }
    }

    for (i = TAB_OFFSET ; i < DIM_X+TAB_OFFSET ; i++)
    {
        for (j = TAB_OFFSET ; j < DIM_Y+TAB_OFFSET ; j++)
        {
            tab[i][j] = res[i][j];
            res[i][j] = false;
        }
    }
    return 0;
}

int main(int argc, char *argv[])
{

    SDL_Surface *ecran = NULL, *pixels[DIM_X][DIM_Y] = {NULL};
    SDL_Rect position;
    int i = 0, j = 0, is_paused = 0;
    bool tableau[DIM_X+(TAB_OFFSET*2)][DIM_Y+(TAB_OFFSET*2)] = {false};
    bool (*tab_ptr)[DIM_Y+(TAB_OFFSET*2)] = tableau;
    bool tableau_result[DIM_X+(TAB_OFFSET*2)][DIM_Y+(TAB_OFFSET*2)] = {false};
    bool (*tab_res_ptr)[DIM_Y+(TAB_OFFSET*2)] = tableau_result;
    long count = 0;
    pixel clicked_pixel = {0, 0};

    for (i = 0 ; i < DIM_X+(TAB_OFFSET*2) ; i++)
        for (j = 0 ; j < DIM_Y+(TAB_OFFSET*2) ; j++)
            tab_ptr[i][j] = false;

    SDL_Event event;
    int continuer = 1;
    int tempsPrecedent = 0, tempsActuel = 0;

    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_WM_SetCaption("Automate Cellulaire OKLM!", NULL);
    ecran = SDL_SetVideoMode(DIM_X*PXL_X, DIM_Y*PXL_Y, 32, SDL_HWSURFACE);

    for (i = 0 ; i < DIM_X ; i++)
        for (j = 0 ; j < DIM_Y ; j++)
            pixels[i][j] = SDL_CreateRGBSurface(SDL_HWSURFACE, PXL_X, PXL_Y, 32, 0, 0, 0, 0);

    while (continuer)
    {
        SDL_PollEvent(&event);
        SDL_PumpEvents();
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = 0;
                break;
            case SDL_KEYDOWN:
                if( event.key.keysym.sym == SDLK_SPACE)
                {
                    if( is_paused == 1)
                    {
                        is_paused = 0;
                    }
                    else
                    {
                        is_paused = 1;
                    }
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
                if( event.button.button == SDL_BUTTON_LEFT)
                {
                    if((event.button.x != 65535) && (event.button.y != 65535))
                    {
                        printf("%d-%d\n",(event.button.x/PXL_X)+TAB_OFFSET,(event.button.y/PXL_Y)+TAB_OFFSET);
                        if(tab_ptr[(event.button.x/PXL_X)+TAB_OFFSET][(event.button.y/PXL_Y)+TAB_OFFSET] == true)
                        {
                            tab_ptr[(event.button.x/PXL_X)+TAB_OFFSET][(event.button.y/PXL_Y)+TAB_OFFSET] = false;
                        }
                        else
                        {
                            tab_ptr[(event.button.x/PXL_X)+TAB_OFFSET][(event.button.y/PXL_Y)+TAB_OFFSET] = true;
                        }
                        clicked_pixel.x = event.button.x;
                        clicked_pixel.y = event.button.y;
                    }

                    event.button.x = -1;
                    event.button.y = -1;
                }
                break;
            default:
                break;
        }
        tempsActuel = SDL_GetTicks();
        if ((tempsActuel - tempsPrecedent > FRAMERATE) && is_paused == 0 )
        {
            SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));
            if(processAC2(tab_ptr, tab_res_ptr) == 1)
            {
                break;
            }
            if(count == 0)
            {
                tab_ptr[4][3] = 1;
                tab_ptr[4][4] = 1;
                tab_ptr[4][5] = 1;
                tab_ptr[4][6] = 1;
            }

            for (i = 0 ; i < DIM_X ; i++)
            {
                for (j = 0 ; j < DIM_Y ; j++)
                {
  				          position.x = i*PXL_X;
  				          position.y = j*PXL_Y;
                    //printf("%d-%d\n",position.x,position.y);
                    if(tab_ptr[i+TAB_OFFSET][j+TAB_OFFSET] == true)
                    {
                        SDL_FillRect(pixels[i][j], NULL, SDL_MapRGB(ecran->format, 0, 255, 10));
                    }
                    else
                    {
                        SDL_FillRect(pixels[i][j], NULL, SDL_MapRGB(ecran->format, 0, 55, 0));
                    }
                    SDL_BlitSurface(pixels[i][j], NULL, ecran, &position);
                }
		        }
            count++;
            /*if(count > 10)
            {
                break;
            }*/
            tempsPrecedent = tempsActuel;
        }
        else
		    {
            SDL_Delay(FRAMERATE - (tempsActuel - tempsPrecedent));
		    }
        SDL_Flip(ecran);
    }
    pause();

    for (i = 0 ; i < DIM_X ; i++)
        for (j = 0 ; j < DIM_Y ; j++)
            SDL_FreeSurface(pixels[i][j]);

    SDL_Quit();

    return EXIT_SUCCESS;
}

int processAC1(bool tab[][DIM_Y+(TAB_OFFSET*2)], long count)
{
    if(count < 2)
        return 0;
    int i, j, test;
    for (i = TAB_OFFSET ; i < DIM_Y+TAB_OFFSET ; i++)
    {
        test = tab[i-1][count-1] + tab[i][count-1] + tab[i+1][count-1];
        if( test == 3 )
        {
            tab[i][count] = false;
        }
        else if( test == 2 )
        {
            if( tab[i-1][count-1] == 1 )
            {
                tab[i][count] = false;
            }
            else
            {
                tab[i][count] = true;
            }
        }
        else if( test == 0 )
        {
            tab[i][count] = false;
        }
        else
        {
            tab[i][count] = true;
        }
    }
    if( count > DIM_Y )
    {
        printf("Done!\n");
        return 1;
    }
    else
    {
        return 0;
    }
}

void pause()
{
    int continuer = 1;
    SDL_Event event;

    while (continuer)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = 0;
        }
    }
}
